+++
title = "Lifestyles For Change"
date = 2019-07-22
sort_by = "date"
render = true
+++

## *BioPhilia Living Design ~ Ancient Technologies For A Modern World
##### *Choosing to relate to nature as a source from which to conduct ones life.*

##### Transition from globalization to eco-localism. Cultivating ecological literacy, connecting soil to soul.*

##### *Vission: Helping WoMen make a living by Creating A Legacy With Nature.*

#### EnvironMental Health Is Real ~ *Heal The Soil Heal The Soul.*
**Discovering and sharing solutions to everyday environMental impacts that we inflict on ourselves and our surroundings. Creating healthy living environments, turning the tables from an environMental withdraw to a environMental deposit for the future.**

#### *Sustainable Futures - Leave A Legacy That Grows.*
**Creating interactive living spaces to explore, learn, reflect, ... on how we interact, impact and live in our personal environments; homes, work, travel, ..., spaces. A place to reconnect with the Self, Nature, Community and other Changemakers.**

### *Private Consulting* ~ Experience workshops and public speaking.
#### **Anna - Creative Eco-Living Lifestyle Designer and Changemaker.**
**It's an artist's duty to reflect the era in which we live. Valuing life, relationships, experiences, and soul-care. Working privately with individuals and small groups, connect and discover the power of BioPhilia; Love of Nature.**

#### *We Are Our Ancestors Wildest Dreams.*
**Bio*Philia* Living Design - Beauty as a basic: Widening our circle of compassion to embrace all living creatures and the whole of nature in its beauty.** [*BioPhilia Living Design Details Here*](https://gitlab.com/anna.bird/www.moringa.im/-/blob/master/Home/bioPhilia_Living_Design.md)

#### *An Opportunity For The Creation Of Something New And Beautiful.*
** We can deny the amazing possibilities that are before us only by keeping ourselves ignorant of them. It's time to step out from the story of our present situation, and write a New Future, a New Story about healing. Creating space for a kind of deprogramming from the 'modern story' and an immersion into a new and ancient story.**

#### *Passionate about protecting the natural world in which we all live.*
**I take responsibility for the way I use all that is available to me. I strive to understand and interact with the natural world with consideration for those here now and those who will follow. I feel a deep moral obligation to respect, preserve, and restore our natural environment and resources when and where I can.**

#### *Palpable Energy* 
**Actively invest in and share my background, unique experiences, network and resources into What I do and Whom I work with. To inspire, nourish and support environmental creativity toward developing the potential given by Nature.**

[**Public Profile & Reviews**](https://www.airbnb.com/users/show/3774308)

[Chat with me directly](https://matrix.to/#/!ibYXXCkubbZiWtkmhX:matrix.org?via=matrix.org)

**Instagram - @vivarium_place_of_life**

**Twitter - @Emulate_Nature**

**[YouTube](https://www.youtube.com/channel/UCJsiffL5swNOi37qDOsk8ZQ/videos)** 

#### [*Support This Work With A Donation*](https://www.paypal.me/vivarium) With every donation a moringa tree will be planted and cared for in your honour.

#### *We shall come to honor all of life sooner or later. Our choices are; when that shall happen, and the quality of experience that we shall have as we learn.*


